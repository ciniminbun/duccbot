import requests as r
from datetime import datetime
import json
import random

# requests setup
default_timeout = 10
logTime = datetime.now()
headers =  {
    'User-Agent': 'duccbot Discord bot by cinimin',
    'From': 'ciniminbun@gmail.com',
    }

def search(search):
    url = f"https://api.giphy.com/v1/gifs/search?api_key=mJqpPSKZjAgPfdOZkZ7QN8V5BdPvr5fH&q={search}&limit=1&lang=en"
    gifs = json.loads(r.get(url, headers=headers).text)
    return gifs['data'][0]['url']

def searchRnd(search):
    url = f"https://api.giphy.com/v1/gifs/search?api_key=mJqpPSKZjAgPfdOZkZ7QN8V5BdPvr5fH&q={search}&limit=50&lang=en"
    gifs = json.loads(r.get(url, headers=headers).text)
    return gifs['data'][random.randrange(50)]['url']
