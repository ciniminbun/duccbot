import discord
from discord.ext import commands
from discord.ext.commands.core import command
import e6API

embedColor = 6414909

categories = {"gen": 0, "spe": 1, "cha": 2, "art": 3, "met": 4}

def embedDefault(title, desc, author, icon, image, footer, foot_icon, author_url):
    embed = discord.Embed(description=desc, color=embedColor, title=title)
    embed.set_author(name=author, icon_url=icon, url=author_url)
    embed.set_footer(text=footer, icon_url=foot_icon)
    embed.set_image(url=image)
    return embed

def get_score(desc):
    data = e6API.make_call(f'order:random {desc}', 1, 320, 2)
    try:
        avg = format(e6API.average(data), '.2f')
    except UnboundLocalError:
        avg = 'no data'
    try:
        med = e6API.median(data)
    except:
        med = ''
    try:
        count = e6API.count(data)
    except:
        count = ''

    score = f'**Average:** {str(avg)} | **Median:** {str(med)} | **Total:** {str(count)}'

    if avg != 'no data':
        result = score
    else:
        result = 'no data'
    return result

def versus(ctx, args):
    print (args)
    args = args.split(' vs ')
    print(args)
    if len(args) < 2:
        return "⚠ Incorrect formatting, use 'd.help e6' to reference syntax"

    scores = [0.0 ,0.0]
    for i in range(0,2):
        data = e6API.make_call(f'order:random {args[i]}', 1, 200, 3)
        try:
            avg = float(format(e6API.average(data), '.2f'))
        except UnboundLocalError:
            avg = 0
        scores[i] = avg

    if scores[0] >= scores[1]:
        winner = 0
        loser = 1
    else:
        winner = 1
        loser = 0

    post = str(e6API.simple_call(f"{args[winner]} order:random"))
    post_url = f'https://e621.net/posts/{post}'

    title = ''
    desc = f"**{args[winner]}:** {str(scores[winner])} | **{args[loser]}:** {str(scores[loser])}\n**Winner: **[**Link**]({post_url})"
    author = f"{args[winner]} is more popular than {args[loser]}"
    icon = ctx.author.avatar_url
    image = e6API.img_from_id(post)
    footer = 'e621'
    foot_icon = 'https://i.imgur.com/RrHrSOi.png'
    author_url = post_url
    em = embedDefault(title, desc, author, icon, image, footer, foot_icon, author_url)
    return em

class E621Commands(commands.Cog):

    def __init__(self, client):
        self.client = client
    
    # Commands
    @commands.group(invoke_without_command=True)
    async def e6(self, ctx, *, args):
        if not ctx.channel.is_nsfw():
            await ctx.reply('⚠ Please use this command in an NSFW channel', mention_author=False)
            return
        if not args:
            await ctx.reply('⚠ i need texto pleaso', mention_author=False)
            return

        # Data (default)
        print(f"getting e6 for {str(ctx.author)} ...")
        async with ctx.typing():
            data = e6API.make_call(f"{args} order:random", 1, 1, 1)[0]['posts']
            post = str(data[0]["id"])
            image = e6API.img_from_id(post)
            score = f"Score: {data[0]['score']['total']} | Resolution: {data[0]['file']['width']} x {data[0]['file']['height']} | [**Link**](https://e621.net/posts/{post})"
            icon = ctx.author.avatar_url
            url_arg = e6API.format_tags(args)
            author_url = f'https://e621.net/posts?desc={url_arg}'
            footer = 'e621'
            footicon = 'https://i.imgur.com/RrHrSOi.png'
            result = embedDefault('', score, args, icon, image, footer, footicon, author_url)
        
        if type(result) == str:
            await ctx.reply(result)
        else:
            await ctx.reply(embed=result, mention_author=False)

    @e6.command()
    async def dat(self, ctx, *args):
        if not ctx.channel.is_nsfw():
            await ctx.reply('⚠ Please use this command in an NSFW channel', mention_author=False)
            return
        if not args:
            await ctx.reply('⚠ i need texto pleaso', mention_author=False)
            return

        # Check for Data or Versus command
        if " vs " not in args:
            # Data (default)
            print(f"getting e6 for {str(ctx.author)} ...")
            async with ctx.typing():
                post = str(e6API.simple_call(f"{args} order:random"))
                image = e6API.img_from_id(post)
                score = f"{get_score(args)}\n[**Link**](https://e621.net/posts/{post})"
                icon = ctx.author.avatar_url
                url_arg = e6API.format_tags(args)
                author_url = f'https://e621.net/posts?desc={url_arg}'
                footer = 'e621'
                footicon = 'https://i.imgur.com/RrHrSOi.png'
                result = embedDefault('', score, args, icon, image, footer, footicon, author_url)
        else:
            #Versus
            result = versus(ctx, args)
        
        if type(result) == str:
            await ctx.reply(result)
        else:
            await ctx.reply(embed=result, mention_author=False)


    @e6.command()
    async def sug(self, ctx, *args):
        if not args:
            await ctx.reply('⚠ i need texto pleaso', mention_author=False)
            return
        if not ctx.channel.is_nsfw():
            await ctx.reply('⚠ Please use this command in an NSFW channel', mention_author=False)
            return

        async with ctx.typing():
            if len(args) > 1:
                args = ' '.join(args).split(', ')
                username = args[0]
                tags = args[1]
                if len(args) > 2:
                    try:
                        cat_choice = categories[args[2]]
                    except KeyError:
                        await ctx.reply('⚠ Category choice error\noptions: gen, spe, cha, art, met', mention_author=False)
                        return
                else:
                    cat_choice = 0
            else:
                username = args[0]
                tags = ""
                cat_choice = 0
            
            data = e6API.make_call(f'order:random fav:{username}', 1, 320, 1)
            post_id, sug_tags = e6API.make_suggestion(data, tags, 30, 5, cat_choice)
            if post_id == "timeout":
                await ctx.reply(f'⚠ Error: {post_id}', mention_author=False)
                return
            else:
                post_url = "https://e621.net/posts/" + post_id

            if tags == "":
                tags = "`none`"

            author = f"suggestion for {username}"
            title = ""
            image = e6API.img_from_id(post_id)
            desc = f"**Suggested Tags:** `{', '.join(sug_tags)}`\n**Manual Input:** {tags}\n[**Link**]({post_url})"
            icon = ctx.author.avatar_url
            author_url = post_url
            footer = 'e621'
            footicon = 'https://i.imgur.com/RrHrSOi.png'
            await ctx.reply(embed=embedDefault(title, desc, author, icon, image, footer, footicon, author_url), mention_author=False)
    
    @e6.command()
    async def top(self, ctx, *args):
        if not args:
            await ctx.reply('⚠ i need texto pleaso', mention_author=False)
            return
        if not ctx.channel.is_nsfw():
            await ctx.reply('⚠ Please use this command in an NSFW channel', mention_author=False)
            return

        async with ctx.typing():
            args = ' '.join(args).split(', ')
            if len(args) > 1:
                username = args[0]
                try:
                    cat_choice = categories[args[1]]
                except KeyError:
                    await ctx.reply('⚠ Category choice error\noptions: gen, spe, cha, art, met', mention_author=False)
                    return
            else:
                username = args[0]
                cat_choice = 0

            data = e6API.make_call(f'order:random fav:{username}', 1, 320, 1)
            tag_list = e6API.dictionary_order(e6API.user_stats_clean(e6API.tagstats(data, cat_choice)), 20)
            post_id = str(e6API.simple_call(f"order:random {tag_list[0]}"))

            author = f"top tags for {username}"
            title = ""
            image = e6API.img_from_id(post_id)
            desc = ', '.join(tag_list).replace("_", " ")
            icon = ctx.author.avatar_url
            author_url = f"https://e621.net/posts/{post_id}"
            footer = 'e621'
            footicon = 'https://i.imgur.com/RrHrSOi.png'
            await ctx.reply(embed=embedDefault(title, desc, author, icon, image, footer, footicon, author_url), mention_author=False)

def setup(client):
    client.add_cog(E621Commands(client))