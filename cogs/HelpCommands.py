import discord
from discord.ext import commands
from discord.ext.commands.core import command
import EmbedTemplates

embedColor = EmbedTemplates.color()

class HelpCommands(commands.Cog):

    def __init__(self, client):
        self.client = client
    
    # Commands
    @commands.group(invoke_without_command=True)
    async def help(self, ctx):
        em = discord.Embed(title = "Duccbot Helpppppp", description = "Use d.help <command> to find out more about a command", color = embedColor)
        em.add_field(name = "Fun <:shoesukeFREEZE:778057950723178506>", value="dan, say, media")
        em.add_field(name = "API <:hooman2:594446982240796682>", value="graph, tti, dog, fact")
        em.add_field(name = "NSFW <:mfw_bakingsoda:777658839305682955>", value="e6")
        await ctx.send(embed=em)

    @help.command()
    async def dog(self, ctx):
        em = discord.Embed(title = "dog", description = "sends random dog", color = embedColor)
        em.add_field(name = "Syntax", value="none")
        await ctx.send(embed=em)

    @help.command()
    async def media(self, ctx):
        em = discord.Embed(title = "media", description = "sends videos and images\nleaving input blank sends random media", color = embedColor)
        em.add_field(name = "Syntax", value="\nd.media list <page number>\nd.media <filename>")
        await ctx.send(embed=em)

    @help.command()
    async def tti(self, ctx):
        em = discord.Embed(title = "tti", description = "requests an image generated from DeepAI's Text to Image", color = embedColor)
        em.add_field(name = "Syntax", value="d.tti <text>")
        await ctx.send(embed=em)

    @help.command()
    async def dan(self, ctx):
        em = discord.Embed(title = "dan", description = "talk to DanBot", color = embedColor)
        em.add_field(name = "Syntax", value="d.dan <message/question>")
        await ctx.send(embed=em)

    @help.command()
    async def fact(self, ctx):
        em = discord.Embed(title = "fact", description = "sends random fact", color = embedColor)
        em.add_field(name = "Syntax", value="none")
        await ctx.send(embed=em)

    @help.command()
    async def say(self, ctx):
        em = discord.Embed(title = "say", description = "repeats message back to you\nuse saydel to have your message autodeleted", color = embedColor)
        em.add_field(name = "Syntax", value="d.say <message>")
        await ctx.send(embed=em)

    @help.command()
    async def saydel(self, ctx):
        em = discord.Embed(title = "saydel", description = "repeats message back to you and deletes your message", color = embedColor)
        em.add_field(name = "Syntax", value="d.saydel <message>")
        await ctx.send(embed=em)

    @help.command()
    async def e6(self, ctx):
        em = discord.Embed(title = "e6", description = "gets images and info from e621.net", color = embedColor)
        em.add_field(name = "Syntax", value="d.e6 <tags>\nd.e6 <tag1> vs <tag2>\nd.e6 sug <user>, <tags>*\nd.e6 top <user>, <tag category>*\n\n`tag categories are gen (general), spe (species), cha (character), art (artist), and met (meta)`\n`*optional input`")
        await ctx.send(embed=em)

    @help.command()
    async def aviegun(self, ctx):
        await ctx.send("<:daviegun:728151313468358707>")
    
    @help.command()
    async def graph(self, ctx):
        em = discord.Embed(title = "graph", description = "creates a graph with quickchart.io\nTypes: `pie, line, bar`", color = embedColor)
        em.add_field(name = "Syntax", value="d.graph <type> <title>, <label>:<value> <label>:<value>")
        await ctx.send(embed=em)
    
    @commands.Cog.listener()
    async def on_command_error(self, ctx: commands.Context, error: commands.CommandError):
        """A global error handler cog."""

        if isinstance(error, commands.CommandNotFound):
            badcom = str(error).replace('Command "', "").replace('" is not found', "")[:-1] + "imer"
            message = f"d.{badcom}"
        elif isinstance(error, commands.CommandOnCooldown):
            message = f"slow down ashol giv it {round(error.retry_after, 1)} seconds"
        elif isinstance(error, commands.MissingPermissions):
            message = "not allowed, ur gay"
        elif isinstance(error, commands.UserInputError):
            message = "bad input >:("
        else:
            message = "broekn :C"

        await ctx.send(message)


def setup(client):
    client.add_cog(HelpCommands(client))