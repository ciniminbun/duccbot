import discord
from discord.ext import commands
from discord.ext.commands.core import command
import os
from random import *
import random
import math
import EmbedTemplates
import DiceRolling
import time

# mediaDict = {
#   "filename": ["file extension", bool]
# }
mediaList = os.listdir("media")
mediaDict = {}
def resetMedia():
    global mediaList
    mediaList = os.listdir("media")
    for file in mediaList:
        mediaDict[file.split(".")[0]] = [file.split(".")[1], False]
    print("Media Dictionary Reset")
resetMedia()

danDict = {}
usable_lines = []
def resetDan():
    global danDict
    danDict = {}
    danTxt = open("danbotquotes.txt", "r", encoding='utf-8')
    danList = danTxt.readlines()
    for i in danList:
        danDict[i] = False
    print("Danbot Reset")
    return danDict
resetDan()

def listMedia(page):
        global mediaDict
        try:
            page = int(page)
        except TypeError:
            return "⚠ not a valid page number"
        nameList = list(mediaDict.keys())
        printList = ""
        perpage = 10
        totalPages = math.ceil(len(nameList) / perpage)

        if page not in range(1, totalPages + 1):
            return "⚠ page does not exist"
        else:
            for i in range(page * perpage - perpage + 1, page * perpage + 1):
                try:
                    printList = printList + nameList[i - 1] + "\n"
                except IndexError:
                    break

            em = discord.Embed(title = "", description = f"**Media List**\n{printList}", color = EmbedTemplates.color())
            em.set_footer(text=f"Page {str(page)} of {str(totalPages)}", icon_url='')
            return em

class FunCommands(commands.Cog):

    def __init__(self, client):
        self.client = client
    
    # Commands
    @commands.command(pass_context = True)
    async def say(self, ctx, *args):
        if not args:
            await ctx.reply("⚠ i need texto pleaso", mention_author=False)
        else:
            await ctx.send(" ".join(args))

    @commands.command(pass_context = True)
    async def saydel(self, ctx, *args):
        if not args:
            await ctx.reply("⚠ i need texto pleaso", mention_author=False)
        else:
            await ctx.send(" ".join(args))
            await ctx.message.delete()
        
    @commands.command(pass_context = True)
    async def media(self, ctx, *args):
        global mediaDict
        if not args:
            needs_reset = True
            for i in mediaDict:
                if mediaDict[i][1] == False:
                    needs_reset = False
            if needs_reset == True:
                print("Resetting Media Dictionary")
                resetMedia()
            
            counter = 0
            entry_list = list(mediaDict.items())
            while True:
                name = random.choice(entry_list)[0]
                if mediaDict[name][1] == False or counter >= len(mediaDict):
                    break
                else:
                    counter += 1
            mediaDict[name][1] = True
            result = f"{name}.{mediaDict[name][0]}"
        
        elif args[0] == "list":
            if len(args) > 1:
                result = listMedia(args[1])
            else:
                result = listMedia(1)
            if type(result) != str:
                await ctx.send(embed=result)
        else:
            name = ' '.join(args)
            if name in mediaDict:
                result = f"{name}.{mediaDict[name][0]}"
            else:
                result = "⚠ file not found"

        if "⚠" in result:
            await ctx.send(result)
        else:
            await ctx.send(name, file=discord.File(f"media/{result}"))

    @commands.command()
    async def dan(self, ctx):
        def get_lines():
            global usable_lines
            usable_lines = []
            for key in danDict:
                if danDict[key] == False:
                    usable_lines.append(key)

        get_lines()
        if len(usable_lines) == 0:
            print("Resetting Danbot Dictionary...")
            resetDan()
            get_lines()
        
        counter = 0
        while True:
            name = random.choice(usable_lines)
            if danDict[name] == False or counter >= len(danDict):
                break
            else:
                counter += 1
        danDict[name] = True
        result = f"{name}".replace('\\n', '\n').encode("ascii", "ignore").decode()

        desc = result
        icon = 'https://cdn.discordapp.com/emojis/816931042761768980.png?v=1'
        author = 'DanBot'
        em = EmbedTemplates.default('', desc, author, icon, '', '', '', '')
        
        await ctx.send(embed=em)

    @commands.command()
    async def aviegun(self, ctx):
        await ctx.send("https://cdn.discordapp.com/emojis/728151313468358707.png?v=1")

    @commands.command()
    async def onk(self, ctx):
        await ctx.send("https://media.discordapp.net/attachments/750923165936517190/890829758991056946/donk.png")

    @commands.command()
    async def supershape(self, ctx):
        await ctx.reply("no")

    @commands.command()
    async def roll(self, ctx, *args):
        if args[0] == "ex":
            result = "too lazy to make this work rn ask me later"
            # resultlist = DiceRolling.ExplodeList(args[1])
            # result = f"**{ctx.author.name}** rolls **{resultlist[0]}** with **{resultlist[1]}** pos explosions and **{resultlist[2]}** neg explosions"
        elif args[0] == "gam":
            result = f"**{ctx.author.name}** rolls **{str(DiceRolling.theGamogEquation())}** (gamogi)"
        else:
            result = f"**{ctx.author.name}** rolls **{str(DiceRolling.rollDice(args[0]))}** (1-{args[0]})"
        
        await ctx.send(result)

            

def setup(client):
    client.add_cog(FunCommands(client))