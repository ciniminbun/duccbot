import discord
from discord.ext import commands
from discord.ext.commands.core import command
import os
import quickchart as QuickChart
import numpy as np
import requests as r
import json
import EmbedTemplates
import urllib.request as dl
from datetime import datetime
import giphyAPI as gif

# requests setup
default_timeout = 10
logTime = datetime.now()
headers = {
    'User-Agent': 'duccbot Discord bot by cinimin',
    'From': 'ciniminbun@gmail.com',
}

def unpack(filepath):
    with open(filepath, 'r') as f:
        raw = f.read()
    j_data = json.loads(raw)
    return j_data

def cacheClear():
    try:
        cache_list = os.listdir(".cache")
    except FileNotFoundError:
        os.mkdir(".cache")
        cache_list = os.listdir(".cache")
    for i in cache_list:
        os.remove(f".cache/{i}")

def cacheGraph(graph_type, args):
        # EX: args = "Poo, hello:1 world:2 test:3"
        args = " ".join(args).split(", ")
        graph_data = args[1].split(" ")
        for i in range(0, len(graph_data)):
            if "_" in graph_data[i]:
                graph_data[i] = graph_data[i].replace('_', ' ')
            graph_data[i] = graph_data[i].split(":")
            
        title = args[0]
        labels = []
        values = []
        for pair in graph_data:
            labels.append(pair[0])
            values.append(pair[1])

        qc = QuickChart()
        qc.width = 500
        qc.height = 300
        qc.device_pixel_ratio = 2.0

        rgb = list(np.random.choice(range(256), size=3))

        if graph_type == "bar":
            qc.config = {
                "type": graph_type,
                "data": {
                    "labels": labels,
                    "datasets": [{
                        "label": title,
                        "data": values,
                        "backgroundColor": f'rgb({rgb[0]},{rgb[1]},{rgb[2]})',
                    }]
                }
            }
        elif graph_type == "line":
            qc.config = {
                "type": graph_type,
                "data": {
                    "labels": labels,
                    "datasets": [{
                        "label": title,
                        "data": values,
                        "fill": False,
                        "borderColor": f'rgb({rgb[0]},{rgb[1]},{rgb[2]})',
                    }]
                }
            }
        elif graph_type == "outlabeledPie":
            pie_color = [f'rgb({rgb[0]},{rgb[1]},{rgb[2]})']
            for i in range(0,len(labels)):
                rgb = list(np.random.choice(range(256), size=3))
                pie_color.append(f'rgb({rgb[0]},{rgb[1]},{rgb[2]})')
            qc.config = {
                "type": graph_type,
                "data": {
                    "labels": labels,
                    "datasets": [{
                        "label": title,
                        "data": values,
                        "backgroundColor": pie_color,
                    }]
                },
                "options": {
                    "plugins": {
                    "legend": False,
                    "outlabels": {
                        "text": "%l %p",
                        "color": "white",
                        "stretch": 35,
                        "font": {
                        "resizable": True,
                        "minSize": 12,
                        "maxSize": 18
                        }
                    }
                    }
                }
            }

        qc.to_file('.cache/cached_chart.png')

def minUTCcon(tmin, zonecorrection, is24):
    hour = tmin/60
    min = (hour - int(hour))*60
    hour = np.absolute(hour + zonecorrection)
    if hour < 0:
        hour += 24
    if hour > 24:
        hour -= 24
    
    if is24 == False:
        ampm = "AM"
        if hour > 12:
            hour -= 12
            ampm = "PM"
    else:
        sec = (min - int(min))*60
        if sec < 10:
            ampm = f':0{int(sec)}'
        else:
            ampm = f':{int(sec)}'
    hour = int(float(hour))
    min = int(float(min))

    if min < 10:
        min = f'0{min}'
    time = f"{hour}:{min} {ampm}"
    return time

def mTimeto12(hour, min, sec):
    hour = int(hour)
    min = int(min)
    sec = int(sec)
    ampm = "AM"
    if hour > 12:
        hour -= 12
        ampm = "PM"
    if hour == 0:
        hour = 12
    if sec < 10:
            sec = f'0{sec}'
    if min < 10:
        min = f'0{min}'

    time = f"{hour}:{min}:{sec} {ampm}"
    return time

def SoPoCalc(day, lat, lng, zonecorrection):
    cyear = datetime.now().year
    chour = datetime.utcnow().hour
    cmin = datetime.utcnow().minute
    csec = datetime.utcnow().second

    # based on https://gml.noaa.gov/grad/solcalc/solareqns.PDF

    # zenith is set to be sunrise/sunset degrees
    zenith = np.radians(90.833)

    # leap year check
    if cyear % 4 == 0 and cyear % 100 != 0:
        daysinyear = 366
    else:
        daysinyear = 365
    
    # fracy: in radians
    fracy = ((2 * np.pi) / daysinyear) * (day - 1 + ((chour - 12) / 24))
    # eqtime: in minutes
    eqtime = 229.18 * (0.000075 + 0.001868*np.cos(fracy) - 0.032077*np.sin(fracy) - 0.014615*np.cos(2*fracy) - 0.040849*np.sin(2*fracy))
    # decl: in radians
    decl = 0.006918 - 0.399912*np.cos(fracy) + 0.070257*np.sin(fracy) - 0.006758*np.cos(2*fracy) + 0.000907*np.sin(2*fracy) - 0.002697*np.cos(3*fracy) + 0.00148*np.sin(3*fracy)
    # toff: in minutes
    toff = eqtime + (4 * lng) - (60 * zonecorrection)
    tst = chour*60 + cmin + csec/60 + toff
    ha = (180 * np.arccos( (np.cos(zenith)/np.cos(np.radians(lat))*np.cos(decl)) - (np.tan(np.radians(lat))*np.tan(decl)) )) / np.pi
    sunrise = 720 - 4*(lng + ha) - eqtime
    sunset = 720 - 4*(lng - ha) - eqtime
    return sunrise, sunset


class APICommands(commands.Cog):

    def __init__(self, client):
        self.client = client
    
    # Commands
    @commands.group(invoke_without_command=True)
    async def graph(self, ctx, *args):
        # d.graph <type> title: labels labels labels, 1 2 3    

        await ctx.send("suck my weenor")

    @graph.command()
    async def bar(self, ctx, *args):
        # d.graph <type> title: labels labels labels, 1 2 3
        graph_type = "bar"
        cacheGraph(graph_type, args)
        await ctx.send(file=discord.File(".cache/cached_chart.png"))

    @graph.command()
    async def line(self, ctx, *args):
        # d.graph <type> title: labels labels labels, 1 2 3
        graph_type = "line"
        cacheGraph(graph_type, args)
        await ctx.send(file=discord.File(".cache/cached_chart.png"))

    @graph.command()
    async def pie(self, ctx, *args):
        # d.graph <type> title: labels labels labels, 1 2 3
        graph_type = "outlabeledPie"
        cacheGraph(graph_type, args)
        await ctx.send(file=discord.File(".cache/cached_chart.png"))


    @commands.command()
    async def dog(self, ctx):
        print(f"getting dog for {str(ctx.author)} ...")
        data = json.loads(r.get("https://dog.ceo/api/breeds/image/random", headers=headers).text)
        print("... dog obtained")
        dogurl = data['message']
        embed = EmbedTemplates.default('Dog Get!!', f"{str(ctx.author.name)} has requested dog", 'dog time', ctx.author.avatar_url,
                            dogurl, '', '', dogurl)
        await ctx.send(embed=embed)


    @commands.command()
    async def tti(self, ctx, *, arg):
        cacheClear()
        print(f"getting {arg} for {str(ctx.author)} ...")
        deepHeaders = {
        'User-Agent': 'duccbot Discord bot by cinimin',
        'From': 'ciniminbun@gmail.com',
        'api-key': '83ca9fbc-05e7-4809-9060-6e749da51294'
        }
        data = json.loads(r.post("https://api.deepai.org/api/text2img", data={'text': arg}, headers=deepHeaders).text)
        print("... DeepAI image obtained")
        if 'err' in data:
            await ctx.send(data['err'])
        else:
            imageurl = data['output_url']
            file_extension = imageurl.split(".")[3]
            dl.urlretrieve(imageurl, f'.cache/cached_image.{file_extension}')
            result = f"🧠 **DeepAI**  |  {str(ctx.author.name)} has requested {arg}"
            await ctx.send(result, file=discord.File(f'.cache/cached_image.{file_extension}'))


    @commands.command()
    async def deep(self, ctx):
        embed = EmbedTemplates.default('DeepAI API Access', '**Available Commands**\n―――――――――――――\n**tti** - Text to Image\nidk suggest more', '', '', '', 'DeepAI', 'https://deepai.org/api-docs/images/logo.png', 'https://deepai.org/')
        await ctx.send(embed=embed)


    @commands.command()
    async def fact(self, ctx):
        print(f"getting fact for {str(ctx.author)} ...")
        data = json.loads(r.get("https://uselessfacts.jsph.pl/random.json?language=en", headers=headers).text)
        print("... fact obtained")
        rndfact = data['text']
        embed = EmbedTemplates.default(f"{str(ctx.author.name)}'s fact is:", rndfact, 'Fact Get!!', ctx.author.avatar_url,
                            '', '', '', '')
        await ctx.send(embed=embed)

    @commands.command()
    async def time(self, ctx, *, args):
        locations = unpack("json/cityloc.json")
        if "-" not in args:
            try:
                cityloc = locations[args.lower()]
            except KeyError:
                await ctx.send(f"⚠ City of {args.capitalize()} not found")
                return
            url = f"http://worldtimeapi.org/api/timezone/{cityloc['zone']}"
            akdt = json.loads(r.get(url, headers=headers).text)
            citydata = {
                "lat": cityloc["lat"],
                "lng": cityloc["lon"],
                "doy": akdt['day_of_year'],
                "zonecorrection": int(akdt['utc_offset'].split(":")[0])
            }

            sunrise, sunset = SoPoCalc(citydata["doy"], citydata["lat"], citydata["lng"], citydata["zonecorrection"])
            
            zhour = datetime.utcnow().hour + citydata["zonecorrection"]
            if zhour < 0:
                zhour += 24
            if zhour > 24:
                zhour -= 24
            em = EmbedTemplates.easy(f"Current time in {args.capitalize()}:", mTimeto12(zhour, datetime.utcnow().minute, datetime.utcnow().second))
            em.add_field(name = "Sunrise/Sunset", value=f"{minUTCcon(sunrise, citydata['zonecorrection'], False)} / {minUTCcon(sunset, citydata['zonecorrection'], False)}")
            # em.add_field(name = "True Solar Time", value=f"{minUTCcon(tst, 0, True)}")
        else:
            sunrise = []
            sunset = []
            args = args.split(" - ")
            for i in range(0,2):
                try:
                    cityloc = locations[args[i].lower()]
                except KeyError:
                    await ctx.send(f"⚠ City of {args[i].capitalize()} not found")
                    return

                url = f"http://worldtimeapi.org/api/timezone/{cityloc['zone']}"
                akdt = json.loads(r.get(url, headers=headers).text)
                citydata = {
                    "lat": cityloc["lat"],
                    "lng": cityloc["lon"],
                    "doy": akdt['day_of_year'],
                    "zonecorrection": int(akdt['utc_offset'].split(":")[0])
                }
                one, two = SoPoCalc(citydata["doy"], citydata["lat"], citydata["lng"], citydata["zonecorrection"])
                sunrise.append(one)
                sunset.append(two)

            zhour = datetime.utcnow().hour + citydata["zonecorrection"]
            if zhour < 0:
                zhour += 24
            if zhour > 24:
                zhour -= 24
            em = EmbedTemplates.easy(f"Difference in rise/set", f"{args[0]} and {args[1]}")
            em.add_field(name = "Sunrise/Sunset", value=f"{minUTCcon(sunrise[0] - sunrise[1], citydata['zonecorrection'], False)} / {minUTCcon(sunset[0] - sunset[1], citydata['zonecorrection'], False)}")


        await ctx.send(embed=em)
    
    @commands.command()
    async def isitd(self, ctx):
        locations = unpack("json/cityloc.json")
        cityloc = locations["anchorage"]

        url = f"http://worldtimeapi.org/api/timezone/{cityloc['zone']}"
        akdt = json.loads(r.get(url, headers=headers).text)  
        day = akdt['day_of_year']
        zonecorrection = int(akdt['utc_offset'].split(":")[0])
        lat = cityloc["lat"]
        lng = cityloc["lon"]

        sunrise, sunset = SoPoCalc(day, lat, lng, zonecorrection)

        zhour = datetime.utcnow().hour + zonecorrection
        if zhour < 0:
            zhour += 24
        if zhour > 24:
            zhour -= 24

        nowmin = zhour*60 + datetime.utcnow().minute
        if nowmin > sunrise and nowmin < sunset:
            await ctx.send(gif.searchRnd("yes"))
        else:
            await ctx.send(gif.searchRnd("no"))




def setup(client):
    client.add_cog(APICommands(client))