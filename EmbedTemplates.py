import discord

def color():
    return 6414909

def default(title, desc, author, icon, image, footer, foot_icon, author_url):
    embed = discord.Embed(description=desc, color=color(), title=title)
    embed.set_author(name=author, icon_url=icon, url=author_url)
    embed.set_footer(text=footer, icon_url=foot_icon)
    embed.set_image(url=image)
    return embed

def easy(title, desc):
    em = discord.Embed(title = title, description = desc, color = color())
    return em