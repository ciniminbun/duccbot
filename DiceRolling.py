import random

def rollDice(limit):
    return int(random.uniform(1.0, float(limit) + 1.0))

def rollExplode(limit):
    floatLimit = float(limit)
    exploding = True
    total = 0
    temp = 0
    explodeSubCount = 0
    explodeAddCount = 0
    
    dice = rollDice(limit)
    temp = int(dice)
    # print("first roll: " + str(temp))
    total += temp
    if temp == 1:
        while exploding == True:
            dice = rollDice(limit)
            temp = int(dice)
            # print("subtracting: " + str(temp))
            if temp != limit:
                exploding = False
                total -= temp
                explodeSubCount += 1
            else:
                total -= limit
                explodeSubCount += 1
    elif temp == limit:
        while exploding == True:
            dice = rollDice(limit)
            temp = int(dice)
            # print("adding: " + str(temp))
            if temp != limit:
                exploding = False
                total += temp
                explodeAddCount += 1
            else:
                total += temp
                explodeAddCount += 1
    else:
        gallbladder = 0
    print (explodeAddCount, explodeSubCount)

    return total, explodeSubCount, explodeAddCount

def ExplodeList(limit):
    total, negCount, posCount = rollExplode(limit)
    output = [total, negCount, posCount]
    return output

def rollRange(limit, rollRange):
    temp = 0
    roll = 0
    for x in range(rollRange):
        roll = rollDice(limit)
        # print("Rolled: " + str(roll))
        temp += roll
    total = temp
    return total


def theGamogEquation():
    damage = 0
    temp = 0
    rollOne, junkOne, junkTwo = rollExplode(10)
    rollOne += 16
    rollTwo, junkThree, junkFour = rollExplode(10)
    rollTwo += 12
    rollDifference = rollOne - rollTwo
    # print("Roll One: " + str(rollOne) + "\n" + "Roll Two: " + str(rollTwo))

    damage = rollRange(6, 4)

    if rollTwo >= rollOne:
        damage = 0
    elif rollDifference in range(1, 6):
        junkOne += 1
    elif rollDifference in range(7, 9):
        damage += 3
    elif rollDifference in range(10, 12):
        damage += 5
    elif rollDifference in range(13, 14):
        damage += 8
    elif rollDifference >= 15:
        damage += 10
    
    multiplier = rollDice(10)

    if multiplier == 1:
        damage = damage * 3
    elif multiplier in range(2, 4):
        junkOne += 1
    elif multiplier in range(5, 10):
        damage = damage * 0.5
        damage = int(damage)
    
    return int(damage)