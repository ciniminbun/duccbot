import requests
import datetime
import json
import webbrowser
import time
import os
import statistics
from collections import Counter
import heapq
import random

from requests.api import post


# API Functions

# API Setup
url_base = "https://e621.net/posts.json?"
logTime = datetime.datetime.now()
headers = {
    'User-Agent': 'YiffDataScience/1.0 (by cinimin on e621)',
    'From': 'ciniminbun@gmail.com'
}


def format_tags(tags):
    if tags != "":
        temp_tags = tags
        temp_tags = temp_tags.replace(" ", "+")
        temp_tags = temp_tags.replace(":", "%3A")
        temp_tags = temp_tags.replace("=", "%3D")
        temp_tags = "&tags=" + temp_tags
    else:
        temp_tags = ""
    formatted_tags = temp_tags
    return formatted_tags


def page_limits(start_page, limit_page):
    formatted_limits = "limit=" + str(limit_page)
    formatted_limits += "&page=" + str(start_page)
    return formatted_limits


def make_url(tags, start_page, limit_page):
    start_page = str(start_page)
    limit_page = str(limit_page)
    formatted_tags = format_tags(tags)
    formatted_limits = page_limits(start_page, limit_page)
    url = url_base + formatted_limits + formatted_tags
    return url


def make_call(tags, start_page, limit_page, total):
    if start_page == "":
        start_page = 1
    if limit_page == "":
        limit_page = 75
    if total == "":
        total = 1
    else:
        total = int(total)
    url = make_url(tags, start_page, limit_page)

    start_page = int(start_page)
    total = int(total)
    final_data = ""
    total_pages = total - start_page

    # query and call loop
    for start_page in range(start_page, total + 1):
        new_data = ""
        url = make_url(tags, start_page, limit_page)
        print('calling: ' + url)
        r = requests.get(url, headers=headers)

        new_data = r.json()
        new_data = json.dumps(new_data)
        progress = total_pages - (total - start_page)
        if progress > 0:
            final_data += ",\n"
        final_data += new_data
        start_page += 1
        time.sleep(0.6)

    final_data = '[' + final_data + ']'
    final_data = json.loads(final_data)
    return final_data


def save_call(data, tags):
    tags = tags.replace(' ', '_')
    tags = tags.replace(':', '-')
    try:
        os.mkdir("jsonDumps/")
    except OSError as error:
        print(error)
        print("That error is probably just that the dir exists already")

    with open('jsonDumps/' + 'e6Dump_' + tags + '_' + logTime.strftime("%m%d%y_%H%M%S") + '.json', 'w') as json_file:
        json.dump(data, json_file)
    print('json dumped')


def open_url(tags, start_page, limit_page):
    if start_page == "":
        start_page = "1"
    if limit_page == "":
        limit_page = "75"
    formatted_tags = format_tags(tags)
    formatted_limits = page_limits(start_page, limit_page)
    url = url_base + formatted_limits + formatted_tags
    url = url.replace('.json', '')
    print('opening ' + url + 'in browser')
    webbrowser.open(url)


def top_tags_call(page):
    url = f"https://e621.net/tags.json?commit=Search&page={str(page)}&search%5Bhide_empty%5D=yes&search%5Border%5D=count"
    r = requests.get(url, headers=headers)
    data = r.json()
    return data


def simple_call(tags):
    """returns 1 post id from given search"""
    r = requests.get(make_url(tags, 1, 1), headers=headers)
    data = r.json()
    try:
        post_id = data['posts'][0]['id']
    except:
        post_id = ""
    return post_id


def img_from_id(post_id):
    r = requests.get("https://e621.net/posts/" + post_id + ".json", headers=headers)
    data = r.json()
    try:
        url = data['post']['sample']['url']
    except:
        url = "https://orthosurgery.ucsf.edu/sites/default/files/styles/original/public/faculty/Alexis%20Dang.jpeg?itok=shGW5uC8"
    return url


def save_img(url):
    file_extension = url.split(".")[3]
    r = requests.get(url, allow_redirects=True, headers=headers)
    try:
        open('.cache/image_cache.' + file_extension, 'wb').write(r.content)
    except FileNotFoundError:
        os.mkdir(".cache")
        open('.cache/image_cache.' + file_extension, 'wb').write(r.content)

# end


# Statistics

def average(filename):
    """takes a list of strings from a json file and returns the average score"""
    try:
        json_data = filename
    except json.JSONDecodeError as e:
        print('JSON decode ERROR:  data may be corrupt or empty')
    avg_sum = 0
    post_count = 0
    for page_json in json_data:
        try:
            for i in page_json['posts']:
                score = i['score']
                avg_sum = avg_sum + score['total']
                post_count += 1
        except SystemError:
            print("Probably some empty data, so here's what the error would have been:")

    try:
        avg = avg_sum / post_count
    except ZeroDivisionError:
        print('Data may be empty corrupted.')

    return avg


def count(filename):
    """takes a list of strings from a json file and returns the average score"""
    json_data = filename
    post_count = 0
    for page_json in json_data:
        try:
            for i in page_json['posts']:
                post_count += 1
        except SystemError as e:
            print("Probably some empty data, so here's what the error would have been:")
            print(e)
    return post_count


def median(filename):
    """takes a list of strings from a json file and returns the median score"""
    json_data = filename
    scores = []
    for page_json in json_data:
        try:
            for i in page_json['posts']:
                scores.append(i['score']['total'])
        except SystemError as e:
            print("Probably some empty data, so here's what the error would have been:")
            print(e)

    med = statistics.median(scores)
    return med


def unpack(filepath):
    with open(filepath, 'r') as f:
        raw = f.read()
    j_data = json.loads(raw)
    return j_data


def tagstats(filename, option):
    """Makes and returns an ordered dict of all tags from an e6 json"""
    # json_data = unpack(filename)
    json_data = filename
    tag_list = []
    # remember: starts on 0
    categories = ["general", "species", "character", "artist", "meta"]

    for page in json_data:
        for posts in page['posts']:
            for i in posts['tags'][categories[option]]:
                tag_list.append(i)
    tag_list.sort()

    tag_dict = Counter()
    for tag in tag_list:
        tag_dict[tag] += 1

    return tag_dict


def dictionary_order(dictionary, n):
    """returns a list of the top values in a range of n from a dictionary"""
    top_list = heapq.nlargest(n, dictionary, key=dictionary.get)
    return top_list


def user_stats_clean(data):
    """removes the top tags from a user stats data set"""
    blacklist = []
    for page in range(1,3):
        tags_json = top_tags_call(page)
        for tag in tags_json:
            if tag['post_count'] > 100000:
                blacklist.append(tag['name'])

    for bad_tag in blacklist:
        if bad_tag in data:
            data.pop(bad_tag)

    return data


def make_suggestion(data, manual_input, tag_range, tag_number, cat_option):
    """comes up with a random suggestion post; note: very dumb"""
    # initialize some variables
    url = "https://e621.net/posts/"
    blacklist = "-animated -flash -webm -loli -young"
    timeout = 15
    tries = 0

    if cat_option == 3:
        tag_number = 1

    # build needed info
    tag_list = dictionary_order(user_stats_clean(tagstats(data, cat_option)), tag_range)

    # throw shit at the wall until it sticks
    # more specifically choose 3 random tags until theres a hit
    while True:
        tags = random.choices(tag_list, k=tag_number)
        print(f'making suggestion with tags {tags} and manual input {manual_input}')
        formatted_tags = ""
        for i in range( 0, tag_number):
            formatted_tags = formatted_tags + tags[i] + " "
        formatted_tags = formatted_tags + "order:random " + blacklist + " " + manual_input
        post_id = simple_call(formatted_tags)
        tries += 1
        if post_id != "" or tries >= timeout:
            break
    
    if post_id != "":
        post_id = str(post_id)
    elif tries >= timeout:
        post_id = "timeout"
    return post_id, tags

# end
