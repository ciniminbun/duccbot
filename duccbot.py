# duccbot.py
import os
import discord
from discord.ext import commands
from discord.utils import get
from dotenv import load_dotenv
import socket
import requests.packages.urllib3.util.connection as urllib3_cn
import schedule
import scheduledEvents as events

# fix requests ipv6 issues by forcing ipv4
def allowed_gai_family():
    family = socket.AF_INET  # force IPv4
    return family

urllib3_cn.allowed_gai_family = allowed_gai_family

# discord bot setup
# client = discord.Client()
load_dotenv()
TOKEN = os.getenv('TOKEN')
client = commands.Bot(command_prefix='d.')
client.remove_command("help")

for filename in os.listdir("./cogs"):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')
        print(f'loaded {filename}')

@client.event
async def on_ready():
    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name="honk"))
    print(f'{client.user} has connected to Discord!')

@client.command()
async def daily(ctx):
    channel = client.get_channel(885217425954578453)
    await channel.send(file=discord.File(events.dailyVideo()))

# schedule.every().day.at("12:00").do(daily())

client.run(TOKEN)
